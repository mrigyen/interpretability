#!/usr/bin/env python3

import log
import argparse
import torch
from transformers import (
    GPT2Config,
    GPT2LMHeadModel,
    GPT2TokenizerFast,
    TrainingArguments,
    Trainer,
)
from datasets import load_dataset

parser = argparse.ArgumentParser()
parser.add_argument(
    "--data_dir",
    dest="data_dir",
    type=str,
    help="path to directory containing text files where each line is one datapoint",
    required=True,
)
parser.add_argument(
    "--batch_size",
    dest="batch_size",
    type=int,
    help="dataloader batch size",
    required=True,
)
parser.add_argument(
    "--epochs",
    dest="epochs",
    type=int,
    help="number of epochs",
    required=True,
)
parser.add_argument(
    "--save_path",
    dest="save_path",
    type=str,
    help="path to save fine-tuned model",
    required=True,
)

if __name__ == "__main__":
    try:
        __IPYTHON__
        args = parser.parse_args(
            [
                "--data_dir",
                "local/scratch/",
                "--batch_size",
                "4",
                "--epochs",
                "1",
                "--save_path",
                ".",
            ]
        )
    except NameError:
        args = parser.parse_args()

    # refer to https://huggingface.co/transformers/v3.0.2/model_doc/gpt2.html
    config = GPT2Config(
        # "The maximum sequence length that this model might ever be used with."
        n_positions=256,
        # "Dimensionality of the causal mask (usually same as n_positions)."
        n_ctx=256,
        # "Number of hidden layers in the Transformer encoder."
        n_layer=6,
        # "Number of attention heads for each attention layer in the Transformer encoder."
        n_head=6,
    )
    # using GPT2Model means model.generate doesn't work because GPT2LMHeadModel
    # output is of type CausalLMOutputWithCrossAttentions which allows
    # model.generate to work while GPT2Model output is of type
    # BaseModelOutputWithPastAndCrossAttentions which doesn't have the `logits`
    # object which model.generate() expects
    model = GPT2LMHeadModel(config=config)
    if torch.cuda.is_available():
        model.cuda()

    tokenizer = GPT2TokenizerFast.from_pretrained("gpt2")
    tokenizer.pad_token = tokenizer.eos_token

    dataset = load_dataset(
        "text",
        data_dir=args.data_dir,
        data_files={"train": "train/*", "test": "test/*"},
    )
    tokenized_dataset = dataset.map(
        lambda example: {
            "input_ids": tokenizer(
                example["text"],
                padding="max_length",
                max_length=256,
                truncation=True,
                # if you use return_tensors="pt", you get the following error:
                # TypeError: Provided `function` which is applied to all elements of
                # table returns a `dict` of types [<class 'list'>, <class 'list'>,
                # <class 'torch.Tensor'>]. When using `batched=True`, make sure
                # provided `function` returns a `dict` of types like `(<class
                # 'list'>, <class 'numpy.ndarray'>)`.
                #
                # if you use return_tensors="np", it doesn't matter, the type will
                # finally be [Int]
            )["input_ids"],
        },
        batched=True,
    )
    tokenized_dataset = tokenized_dataset.map(
        lambda example: {**example, "labels": example["input_ids"]},
        batched=True,
    )
    log.info(f"tokenized dataset!")

    training_args = TrainingArguments(
        evaluation_strategy="epoch",
        per_gpu_train_batch_size=args.batch_size,
        num_train_epochs=args.epochs,
        fp16=True,
        output_dir=args.save_path,
        save_strategy="epoch",
        log_level="debug",
        logging_first_step=True,
    )
    log.info(f"initialize training_args")
    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=tokenized_dataset["train"],
        eval_dataset=tokenized_dataset["test"],
    )
    log.info(f"initialize trainer")
    trainer.train()
    trainer.save_model(args.save_path)
