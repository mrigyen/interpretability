#!/usr/bin/env python3

import log
import argparse
from transformers import GPT2LMHeadModel, GPT2TokenizerFast
import torch

parser = argparse.ArgumentParser()
parser.add_argument(
    "--positive",
    dest="positive",
    type=str,
    help="path to the saved finetuned positive sentiment model",
    required=True,
)
parser.add_argument(
    "--negative",
    dest="negative",
    type=str,
    help="path to the saved finetuned negative sentiment model",
    required=True,
)
parser.add_argument(
    "--input",
    dest="input",
    type=str,
    help="string to be classified",
    required=False,
)
parser.add_argument(
    "--inputs",
    dest="inputs",
    type=str,
    help="path to file with lines of text samples to be classified",
    required=False,
)


def log_prob_seq(model, token_seq):
    assert isinstance(token_seq, list)
    assert len(token_seq) > 0
    assert isinstance(token_seq[0], int)
    token_seq = torch.tensor(token_seq)
    if torch.cuda.is_available():
        token_seq = token_seq.cuda()
    # log.debug(f"token_seq.shape = {token_seq.shape}")
    output = float(log_prob_seq_tensors_only(model, token_seq))
    assert isinstance(output, float)
    return output


def log_prob_seq_tensors_only(model, token_seq):
    assert len(token_seq.shape) == 1
    logits = model(token_seq, labels=token_seq).logits
    # log.debug(f"logits.shape = {logits.shape}")
    assert len(logits.shape) == 2
    log_probs = torch.log_softmax(logits, dim=1)
    # log.debug(f"log_probs.shape = {log_probs.shape}")
    # this looks insane but it works. the shape of the list comprehension output
    # is a [tensor()], so when you do a sum() you get a tensor(), which you then do a torch.sum() over to get the total log_prob_seq
    list_of_tensors = [
        log_probs[i][token_seq[i + 1]] for i in range(len(token_seq) - 1)
    ]
    # log.debug(f"len(list_of_tensors) = {len(list_of_tensors)}")
    tensor = sum(list_of_tensors)
    # log.debug(f"tensor.shape = {tensor.shape}")
    output = torch.sum(tensor)
    assert isinstance(output, torch.Tensor)
    return output


def log_prob_seq_list(model, token_seq):
    assert isinstance(token_seq, list)
    assert len(token_seq) > 0
    assert isinstance(token_seq[0], int)

    if torch.cuda.is_available():
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")
    token_seq = torch.tensor(token_seq, device=device)
    # log.debug(f"token_seq.shape = {token_seq.shape}")
    output = log_prob_seq_list_tensors_only(model, token_seq, device)
    assert isinstance(output, torch.Tensor)
    assert len(output) == len(token_seq) - 1
    return output


def log_prob_seq_list_true(model, token_seq):
    assert isinstance(token_seq, list)
    assert len(token_seq) > 0
    assert isinstance(token_seq[0], int)

    if torch.cuda.is_available():
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")
    token_seq = torch.tensor(token_seq, device=device)
    # log.debug(f"token_seq.shape = {token_seq.shape}")
    output = log_prob_seq_list_tensors_only(model, token_seq, device)
    output = output.tolist()
    assert isinstance(output, list)
    assert len(output) == len(token_seq) - 1
    return output


def log_prob_seq_list_tensors_only(model, token_seq: torch.Tensor, device):
    """
    takes a token_seq of shape [sequence] and returns a list of log
    probabilities calculated using the model

    log_probs is the log_softmax of the logits that a model outputs
    """
    assert isinstance(token_seq, torch.Tensor)
    assert len(token_seq.shape) == 1

    logits = model(token_seq, labels=token_seq).logits
    # log.debug(f"logits.shape = {logits.shape}")
    assert len(logits.shape) == 2

    log_probs = torch.log_softmax(logits, dim=1)
    # log.debug(f"log_probs.shape = {log_probs.shape}")
    assert len(log_probs.shape) == 2

    # this looks insane but it works. the shape of the list comprehension
    # output is a [tensor()], so when you do a sum() you get a tensor(), which
    # you then do a torch.sum() over to get the total log_prob_seq
    list_of_log_probs = torch.tensor(
        [
            # token_seq[i + 1] gives you the token index of the
            # (i+1)th token
            # log_probs[i] gives the model's prediction of how likely any
            # given token is to be the (i+1)th token
            # log_probs[i][token_seq[i + 1]] therefore gives you the model's
            # prediction of how likely the model believes the next token is
            # the actual token predicted
            log_probs[i][token_seq[i + 1]]
            for i in range(len(token_seq) - 1)
            # basically this is a list of log probability of all tokens
            # except the first one (since a token predicts the next one)
            # We also therefore ignore the last sequence output of the
            # model since we don't have data as to what comes after the last
            # token
        ],
        device=device,
    )
    assert len(list_of_log_probs.shape) == 1
    # log.debug(f"len(list_of_log_probs) = {len(list_of_log_probs)}")
    list_of_cumsum_log_probs = torch.cumsum(list_of_log_probs, dim=-1)

    assert isinstance(list_of_cumsum_log_probs, torch.Tensor)
    assert len(list_of_cumsum_log_probs) == len(token_seq) - 1
    return list_of_cumsum_log_probs


if __name__ == "__main__":
    args = parser.parse_args()
    # check that at least one of input or inputs exists
    assert args.input is not None or args.inputs is not None

    seed = 0
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
    else:
        log.info("device does not support CUDA")

    positive = GPT2LMHeadModel.from_pretrained("gpt2")
    positive.load_state_dict(torch.load(args.positive))
    positive.eval()
    negative = GPT2LMHeadModel.from_pretrained("gpt2")
    negative.load_state_dict(torch.load(args.negative))
    negative.eval()

    if torch.cuda.is_available():
        positive.cuda()
        negative.cuda()

    tokenizer = GPT2TokenizerFast.from_pretrained("gpt2")
    tokenizer.pad_token = tokenizer.eos_token
    to_classify = tokenizer(
        args.input,
        padding="max_length",
        max_length=256,
        truncation=True,
    )["input_ids"]
    log_odds = log_prob_seq_list(positive, to_classify) - log_prob_seq_list(
        negative, to_classify
    )
    prob_positive = torch.sigmoid(torch.tensor(log_odds))
    print(f"P(positive) = {prob_positive}")
    print(f"P(negative) = {1 - prob_positive}")
