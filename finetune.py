#!/usr/bin/env python3

"""
- fine-tune GPT-2-small on positive or negative sentiment dataset
- full fine-tuning, not just LM head
- use the Trainer API
- slice every data sample at paragraph breaks
- token length of every training sample input := 256
"""

import argparse
import pandas
from transformers import GPT2LMHeadModel, GPT2TokenizerFast, Trainer, TrainingArguments
from datasets import Dataset
import torch
import log

parser = argparse.ArgumentParser()
parser.add_argument(
    "--dataset",
    dest="dataset",
    type=str,
    help="path to csv file containing the reviews-sentiment pairs",
    required=True,
)
parser.add_argument(
    "--batch_size",
    dest="batch_size",
    type=int,
    help="dataloader batch size",
    required=True,
)
parser.add_argument(
    "--epochs",
    dest="epochs",
    type=int,
    help="number of epochs",
    required=True,
)
parser.add_argument(
    "--save_path",
    dest="save_path",
    type=str,
    help="path to save fine-tuned model",
    required=True,
)
parser.add_argument(
    "--sentiment",
    dest="sentiment",
    type=str,
    help="describe the sentiment that you aim to fine-tune the model on: (positive | negative)",
    required=True,
)


# dqn uses python3.8
def slice_review_at_br(review: str) -> "list[str]":
    assert isinstance(review, str)
    paragraphs = review.split("<br /><br />")
    return [" ".join(paragraphs[i:]) for i in range(len(paragraphs))]


if __name__ == "__main__":
    args = parser.parse_args()

    seed = 0
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
    else:
        log.info("device does not support CUDA")

    reviews = pandas.read_csv(args.dataset)
    if args.sentiment == "positive":
        # In [11]: positive_reviews.columns
        # Out[11]: Index(['review', 'sentiment'], dtype='object')
        reviews = reviews.where(reviews.sentiment == "positive").dropna()
    elif args.sentiment == "negative":
        reviews = reviews.where(reviews.sentiment == "negative").dropna()
    else:
        raise Exception("args.sentiment value is invalid: (positive | negative)")

    final_reviews = []
    for review in reviews.review.to_list():
        final_reviews = final_reviews + slice_review_at_br(review)
    dataset = Dataset.from_dict({"text": final_reviews})

    tokenizer: GPT2TokenizerFast = GPT2TokenizerFast.from_pretrained("gpt2")
    tokenizer.pad_token = tokenizer.eos_token

    tokenized_dataset = dataset.map(
        lambda example: tokenizer(
            example["text"],
            padding="max_length",
            max_length=256,
            truncation=True,
        ),
        batched=True,
    )
    # clone "input_ids" as "labels"
    tokenized_dataset = tokenized_dataset.map(
        lambda example: {**example, "labels": example["input_ids"]},
        batched=True,
    )

    training_args = TrainingArguments(
        evaluation_strategy="epoch",
        per_gpu_train_batch_size=args.batch_size,
        num_train_epochs=args.epochs,
        fp16=True,
        output_dir=args.save_path,
    )
    trainer = Trainer(
        model=GPT2LMHeadModel.from_pretrained("gpt2"),
        args=training_args,
        train_dataset=tokenized_dataset,
        eval_dataset=tokenized_dataset,
    )
    trainer.train()
    trainer.save_model(args.save_path)
