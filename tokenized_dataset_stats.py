#!/usr/bin/env python3

import log
import argparse
import pandas
from transformers import GPT2TokenizerFast
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument(
    "--dataset",
    dest="dataset",
    type=str,
    help="path to csv file containing the reviews-sentiment pairs",
    required=True,
)


def get_stats(reviews, tokenizer, label=None):
    lengths = [
        len(tokenizer(r, return_attention_mask=False)["input_ids"])
        for r in reviews.review.to_list()
    ]
    average = sum(lengths) / len(lengths)
    log.info(f"{label} | average token length: {np.average(lengths)}")
    log.info(f"{label} | standard deviation token length: {np.std(lengths)}")


if __name__ == "__main__":
    tokenizer = GPT2TokenizerFast.from_pretrained("gpt2")
    args = parser.parse_args()
    reviews = pandas.read_csv(args.dataset)
    get_stats(reviews, tokenizer, label="all reviews")
    get_stats(
        reviews.where(reviews.sentiment == "positive").dropna(),
        tokenizer,
        label="positive reviews",
    )
    get_stats(
        reviews.where(reviews.sentiment == "negative").dropna(),
        tokenizer,
        label="negative reviews",
    )

"""
Results for the IMDB dataset:

all reviews | average token length: 296.24864
all reviews | standard deviation token length: 222.1444803233931
positive reviews | average token length: 298.01664
positive reviews | standard deviation token length: 230.16839132059468
negative reviews | average token length: 294.48064
negative reviews | standard deviation token length: 213.80505201044807
"""
