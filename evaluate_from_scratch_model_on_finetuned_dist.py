#!/usr/bin/env python3

from transformers import GPT2LMHeadModel, GPT2TokenizerFast
import argparse
import torch
import scratch
import matplotlib.pyplot as plt
import classify

parser = argparse.ArgumentParser()

parser.add_argument(
    "--positive",
    dest="positive",
    type=str,
    help="path to the saved finetuned positive sentiment model",
    required=True,
)
parser.add_argument(
    "--negative",
    dest="negative",
    type=str,
    help="path to the saved finetuned negative sentiment model",
    required=True,
)

parser.add_argument(
    "--scratch_model_path",
    dest="scratch_model_path",
    type=str,
    help="path to the saved from scratch model",
    required=True,
)


def plot_halfway_sentiment_scratch_generations_log_odds(
    positive, negative, scratch_model, sentiment: str
):
    if sentiment == "positive":
        sentiment_model = positive
    elif sentiment == "negative":
        sentiment_model = negative
    else:
        raise Exception("invalid sentiment value")

    sentiment_outputs = sentiment_model.generate(
        do_sample=True,
        min_length=128,
        max_length=128,
        no_repeat_ngram_size=5,
        repetition_penalty=2.5,
        temperature=1.0,
        num_return_sequences=500,
    )

    completions = scratch_model.generate(
        sentiment_outputs,
        max_new_tokens=128,
        no_repeat_ngram_size=5,
        repetition_penalty=2.5,
        temperature=1.0,
    )
    list_of_log_odds = [
        (
            classify.log_prob_seq_list_tensors_only(positive, example, device)
            - classify.log_prob_seq_list_tensors_only(negative, example, device)
        ).cpu()
        for example in completions
    ]

    x_axis = [x for x in range(len(list_of_log_odds[0]))]
    [plt.plot(x_axis, element_log_odds) for element_log_odds in list_of_log_odds]
    plt.savefig(f"{sentiment}_log_odds_messy.png")
    plt.close()

    # histogram of log_odds of the last generation
    log_odds = torch.stack(list_of_log_odds)
    plt.hist([e[-1] for e in log_odds])
    plt.savefig(f"{sentiment}_last_log_odds_hist.png")
    plt.close()


if __name__ == "__main__":
    try:
        # check for the existence of this variable
        __IPYTHON__
        # if no error is thrown, then we know that we are in a REPL
        args = parser.parse_args(
            [
                "--positive",
                "./models/gpt-2-small/1659353389/positive/pytorch_model.bin",
                "--negative",
                "./models/gpt-2-small/1659355055/negative/pytorch_model.bin",
                "--scratch_model_path",
                "./train_from_scratch/pytorch_model.bin",
            ]
        )
    except NameError:
        # we aren't in a REPL
        args = parser.parse_args()

    seed = 0
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
    else:
        log.info("device does not support CUDA")
    if torch.cuda.is_available():
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")

    positive = GPT2LMHeadModel.from_pretrained("gpt2")
    positive.load_state_dict(torch.load(args.positive))
    positive.eval()
    negative = GPT2LMHeadModel.from_pretrained("gpt2")
    negative.load_state_dict(torch.load(args.negative))
    negative.eval()

    scratch.load(args.scratch_model_path)

    if torch.cuda.is_available():
        positive.cuda()
        negative.cuda()

    tokenizer = GPT2TokenizerFast.from_pretrained("gpt2")
    tokenizer.pad_token = tokenizer.eos_token

    positive_outputs = positive.generate(
        do_sample=True,
        min_length=128,
        max_length=128,
        no_repeat_ngram_size=5,
        repetition_penalty=2.5,
        temperature=1.0,
        num_return_sequences=500,
    )

    positive_completions = scratch.model.generate(
        positive_outputs,
        max_new_tokens=128,
        no_repeat_ngram_size=5,
        repetition_penalty=2.5,
        temperature=1.0,
    )
    plot_halfway_sentiment_scratch_generations_log_odds(
        positive, negative, scratch.model, "positive"
    )
    plot_halfway_sentiment_scratch_generations_log_odds(
        positive, negative, scratch.model, "negative"
    )
