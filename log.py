#!/usr/bin/env python3
"""
Minimalist and sane interface with the PEP8 breaking (and non idempotent) logging STL module
"""
import logging
import os
import datetime
import sys

LOG_DIR = os.environ.get("LOG_DIR") or "."
GIT_COMMIT_HASH = os.environ.get("GIT_COMMIT_HASH") or ""

filename = (
    LOG_DIR
    + "/"
    + "_".join(["log", str(datetime.datetime.utcnow()), GIT_COMMIT_HASH])
    + ".tsv"
)

log_format = "%(asctime)s\t%(levelname)s\t%(message)s"

logging.basicConfig(
    level=logging.DEBUG,
    filename=filename,
    format=log_format,
)


def info(string):
    print(string)
    logging.info(string)


# workaround because guildai sets logging level higher than debug making it
# harder to log this
debug = logging.debug

info("New run initiated")

step = 0


def scalars(scalars_dict, fun=info):
    """
    Log scalars for guildai / tensorboard
    """
    global step
    assert step is not None
    fun(f"step: {step}")
    step = step + 1
    for scalar in scalars_dict:
        fun(f"{scalar}: {scalars_dict[scalar]}")


def scalars_noinc(scalars_dict):
    """
    Log scalars for guildai / tensorboard
    Do not increment the step
    """
    global step
    assert step is not None
    info(f"step: {step}")
    for scalar in scalars_dict:
        info(f"{scalar}: {scalars_dict[scalar]}")


def log_command_run():
    debug(f"command run: {' '.join(sys.argv)}")


def log_env_vars():
    for key in os.environ:
        debug(f"[env var] {key}={os.environ[key]}")


log_command_run()
log_env_vars()
