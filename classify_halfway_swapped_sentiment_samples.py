#!/usr/bin/env python3
"""
Plot mean and stdev of probabilities and log odds of (25, 256) token sequences,
one generated half from positive finetuned model and the second half generated
from the negative finetuned model, the second vice versa.
"""

import log
import argparse
from transformers import GPT2LMHeadModel, GPT2TokenizerFast
import torch
import classify
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument(
    "--positive",
    dest="positive",
    type=str,
    help="path to the saved finetuned positive sentiment model",
    required=True,
)
parser.add_argument(
    "--negative",
    dest="negative",
    type=str,
    help="path to the saved finetuned negative sentiment model",
    required=True,
)

if __name__ == "__main__":
    try:
        # check for the existence of this variable
        # __IPYTHON__
        # if no error is thrown, then we know that we are in a REPL
        args = parser.parse_args(
            [
                "--positive",
                "./models/gpt-2-small/1659353389/positive/pytorch_model.bin",
                "--negative",
                "./models/gpt-2-small/1659355055/negative/pytorch_model.bin",
            ]
        )
    except NameError:
        # we aren't in a REPL
        args = parser.parse_args()

    seed = 0
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
    else:
        log.info("device does not support CUDA")
    if torch.cuda.is_available():
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")

    positive = GPT2LMHeadModel.from_pretrained("gpt2")
    positive.load_state_dict(torch.load(args.positive))
    positive.eval()
    negative = GPT2LMHeadModel.from_pretrained("gpt2")
    negative.load_state_dict(torch.load(args.negative))
    negative.eval()

    if torch.cuda.is_available():
        positive.cuda()
        negative.cuda()

    tokenizer = GPT2TokenizerFast.from_pretrained("gpt2")
    tokenizer.pad_token = tokenizer.eos_token

    # generate half positive-half negative samples
    p_outputs = positive.generate(
        do_sample=True,
        max_new_tokens=128,
        no_repeat_ngram_size=5,
        repetition_penalty=2.5,
        temperature=1.0,
        num_return_sequences=25,
    )
    pn_outputs = negative.generate(
        p_outputs,
        max_new_tokens=128,
        no_repeat_ngram_size=5,
        repetition_penalty=2.5,
        temperature=1.0,
    )
    log.debug(f"type(pn_outputs) = {type(pn_outputs)}")
    log.debug(f"pn_outputs = {pn_outputs}")
    assert pn_outputs.shape[0] == 25
    list_of_pn_log_odds = [
        classify.log_prob_seq_list_tensors_only(positive, pn_outputs[i], device)
        - classify.log_prob_seq_list_tensors_only(negative, pn_outputs[i], device)
        for i in range(len(pn_outputs))
    ]
    pn_log_odds = torch.stack(list_of_pn_log_odds)

    pn_prob = torch.sigmoid(pn_log_odds).cpu()
    std_prob, mean_prob = torch.std_mean(pn_prob, dim=0)
    x_axis = [x for x in range(len(std_prob))]
    plt.plot(x_axis, mean_prob)
    plt.fill_between(x_axis, mean_prob - std_prob, mean_prob + std_prob, alpha=0.2)
    plt.axis([0, 256, 0, 1])
    plt.savefig("pn_mean_std.png")
    plt.close()

    std_log_odds, mean_log_odds = torch.std_mean(pn_log_odds.cpu(), dim=0)
    x_axis = [x for x in range(len(std_log_odds))]
    plt.plot(x_axis, mean_log_odds)
    plt.fill_between(
        x_axis, mean_log_odds - std_log_odds, mean_log_odds + std_log_odds, alpha=0.2
    )
    plt.savefig("log_odds_pn_mean_std.png")
    plt.close()

    # generate half negative-half positive samples
    n_outputs = negative.generate(
        do_sample=True,
        max_new_tokens=128,
        no_repeat_ngram_size=5,
        repetition_penalty=2.5,
        temperature=1.0,
        num_return_sequences=25,
    )
    np_outputs = positive.generate(
        n_outputs,
        max_new_tokens=128,
        no_repeat_ngram_size=5,
        repetition_penalty=2.5,
        temperature=1.0,
    )
    list_of_np_log_odds = [
        classify.log_prob_seq_list_tensors_only(positive, np_outputs[i], device)
        - classify.log_prob_seq_list_tensors_only(negative, np_outputs[i], device)
        for i in range(len(np_outputs))
    ]
    np_log_odds = torch.stack(list_of_np_log_odds)
    # move to CPU so matplotlib can convert to numpy
    np_prob = torch.sigmoid(np_log_odds).cpu()
    std_prob, mean_prob = torch.std_mean(np_prob, dim=0)
    x_axis = [x for x in range(len(std_prob))]
    plt.plot(x_axis, mean_prob)
    plt.fill_between(x_axis, mean_prob - std_prob, mean_prob + std_prob, alpha=0.2)
    plt.axis([0, 256, 0, 1])
    plt.savefig("np_mean_std.png")
    plt.close()

    std_log_odds, mean_log_odds = torch.std_mean(np_log_odds.cpu(), dim=0)
    x_axis = [x for x in range(len(std_log_odds))]
    plt.plot(x_axis, mean_log_odds)
    plt.fill_between(
        x_axis, mean_log_odds - std_log_odds, mean_log_odds + std_log_odds, alpha=0.2
    )
    plt.savefig("log_odds_np_mean_std.png")
    plt.close()
