#!/usr/bin/env python3
"""
Generate examples from the scratch model to get an overview of what it
generates.
"""

import argparse
from transformers import GPT2LMHeadModel, GPT2TokenizerFast, GPT2Config
import torch
import log
import time
import scratch

parser = argparse.ArgumentParser()

parser.add_argument(
    "--load_path",
    dest="load_path",
    type=str,
    help="path to fine-tuned model",
    required=True,
)

if __name__ == "__main__":
    args = parser.parse_args()

    seed = time.time()
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
    else:
        log.info("device does not support CUDA")

    if torch.cuda.is_available():
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")
    model = scratch.load(args.load_path, device)

    tokenizer = GPT2TokenizerFast.from_pretrained("gpt2")

    # print a few samples
    start = time.time()
    outputs = model.generate(
        do_sample=True,
        min_length=256,
        max_length=256,
        # num_beams=5,
        # early_stopping=True,
        no_repeat_ngram_size=5,
        # length_penalty=0.0,
        # higher repetition_penalty == lower repetition
        repetition_penalty=2.5,
        temperature=1.0,
        # 2048 fails with error [1] with max_length=100
        # 4096 fails with error [2] with max_length=100
        # 1024 fails with error [3] with max_length=300
        # 512 fails with error [4] with max_length=300
        num_return_sequences=500,
    )
    [log.info(tokenizer.decode(output)) for output in outputs]
    log.info(f"time taken = {time.time() - start}")
    # takes 156 seconds for 256 sequences each of which is 300 tokens
    # using model.half() turns this from 156 to 82 seconds, so a halving
    # 512 sequences == 200 seconds

"""
error [1]:
RuntimeError: CUDA out of memory. Tried to allocate 464.00 MiB (GPU 0; 47.54 GiB
total capacity; 45.23 GiB already allocated; 390.56 MiB free; 45.26 GiB reserved
in total by PyTorch) If reserved memory is >> allocated memory try setting
max_split_size_mb to avoid fragmentation.  See documentation for Memory
Management and PYTORCH_CUDA_ALLOC_CONF

[2]:
RuntimeError: CUDA out of memory. Tried to allocate 786.00 MiB (GPU 0; 47.54 GiB
total capacity; 40.42 GiB already allocated; 146.56 MiB free; 45.50 GiB reserved
in total by PyTorch) If reserved memory is >> allocated memory try setting
max_split_size_mb to avoid fragmentation.  See documentation for Memory
Management and PYTORCH_CUDA_ALLOC_CONF

[3]:
RuntimeError: CUDA out of memory. Tried to allocate 468.00 MiB (GPU 0; 47.54 GiB
total capacity; 45.18 GiB already allocated; 448.56 MiB free; 45.21 GiB reserved
in total by PyTorch) If reserved memory is >> allocated memory try setting
max_split_size_mb to avoid fragmentation.  See documentation for Memory
Management and PYTORCH_CUDA_ALLOC_CONF

[4]:
RuntimeError: CUDA out of memory. Tried to allocate 20.00 MiB (GPU 0; 47.54 GiB
total capacity; 45.62 GiB already allocated; 8.56 MiB free; 45.64 GiB reserved
in total by PyTorch) If reserved memory is >> allocated memory try setting
max_split_size_mb to avoid fragmentation.  See documentation for Memory
Management and PYTORCH_CUDA_ALLOC_CONF
"""
