#!/usr/bin/env python3
"""
The desired log odds is calculated with respect to a specific pair of layers in
a from-scratch model, and a specific probe trained on the activations between
these layers.
"""


import argparse
from typing import Dict, List, Tuple
from torch.nn import Linear
from scratch import GPT2LMHeadModel
import torch
import finetuned
import pandas as pd
import log
import scratch
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument(
    "--probes_path",
    dest="probes_path",
    type=str,
    help="path to the csv file containing the weights of the linear probes",
    required=True,
)
parser.add_argument(
    "--scratch_path",
    dest="scratch_path",
    type=str,
    help="path to the bin file containing the scratch model data",
    required=True,
)
parser.add_argument(
    "--positive",
    dest="positive",
    type=str,
    help="path to the saved finetuned positive sentiment model",
    required=True,
)
parser.add_argument(
    "--negative",
    dest="negative",
    type=str,
    help="path to the saved finetuned negative sentiment model",
    required=True,
)


def get_probe_output_mean_std(
    scratch_model: GPT2LMHeadModel,
    sentiment: GPT2LMHeadModel,
    probes: List[Linear],
):
    probe_output = [[] for _ in range(len(probes))]
    for _ in range(10):
        outputs = sentiment.generate(
            do_sample=True,
            max_new_tokens=256,
            no_repeat_ngram_size=5,
            repetition_penalty=2.5,
            temperature=1.0,
            num_return_sequences=25,
        )
        hidden_states: Tuple[torch.Tensor] = scratch_model.forward(
            input_ids=outputs[:, 1:],
            output_hidden_states=True,
            return_dict=True,
        )["hidden_states"]
    for probe_index in range(len(probes)):
        probe_output[probe_index].extend(
            probes[probe_index].forward(hidden_states[probe_index][0]).tolist()
        )
    return [
        (
            np.array(probe_output[i]).mean().item(),
            np.array(probe_output[i]).std().item(),
        )
        for i, _ in enumerate(probe_output)
    ]


def get_desired_log_odds(
    positive: GPT2LMHeadModel,
    negative: GPT2LMHeadModel,
    scratch_model: GPT2LMHeadModel,
    probes: List[Linear],
):
    positive_mean_std = get_probe_output_mean_std(scratch_model, positive, probes)
    negative_mean_std = get_probe_output_mean_std(scratch_model, negative, probes)
    return (positive_mean_std, negative_mean_std)


if __name__ == "__main__":
    args = parser.parse_args()

    seed = 0
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
        device = torch.device("cuda")
    else:
        log.info("device does not support CUDA")
        device = torch.device("cpu")

    positive = finetuned.load_finetuned_model(args.positive, device)
    negative = finetuned.load_finetuned_model(args.negative, device)
    probe_data = pd.read_csv(args.probes_path, header=None)
    num_features = len(probe_data.columns) - 1
    probes = [Linear(num_features, 1) for _ in range(len(probe_data))]
    np_probe_data = probe_data.to_numpy()
    for i, _ in enumerate(probes):
        probes[i].weight = torch.nn.Parameter(
            torch.tensor(
                [np_probe_data[i][:num_features]],
                dtype=torch.float32,
                device=device,
            )
        )
        probes[i].bias = torch.nn.Parameter(
            torch.tensor(
                np_probe_data[i][num_features:],
                device=device,
                dtype=torch.float32,
            )
        )

    scratch_model = scratch.load(args.scratch_path, device)
    list_desired_log_odds: Tuple[List[Tuple[float, float]]] = get_desired_log_odds(
        positive,
        negative,
        scratch_model,
        probes,
    )
    log.info(f"list_desired_log_odds = {list_desired_log_odds}")
