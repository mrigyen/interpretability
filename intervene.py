#!/usr/bin/env python3
"""
load up probes, intervene on samples and check results of interventions
evaluate effectiveness of interventions
"""

import argparse

from transformers.generation_utils import GenerationMixin
from transformers import GPT2TokenizerFast
import scratch
import pandas as pd
import torch
import finetuned
import classify
import log
import matplotlib.pyplot as plt
from tensorboardX import SummaryWriter

writer = SummaryWriter()

parser = argparse.ArgumentParser()
parser.add_argument(
    "--probes_path",
    dest="probes_path",
    type=str,
    help="path to the csv file containing the weights of the linear probes",
    required=True,
)
parser.add_argument(
    "--scratch_path",
    dest="scratch_path",
    type=str,
    help="path to the bin file containing the scratch model data",
    required=True,
)
parser.add_argument(
    "--positive",
    dest="positive",
    type=str,
    help="path to the saved finetuned positive sentiment model",
    required=True,
)
parser.add_argument(
    "--negative",
    dest="negative",
    type=str,
    help="path to the saved finetuned negative sentiment model",
    required=True,
)


class Intervention(torch.nn.Module):
    def __init__(self, probe_data_path, device):
        super(Intervention, self).__init__()
        self.probe = self.get_probe(pd.read_csv(probe_data_path, header=None), device)
        self.device = device

    def forward(self, activations, desired_log_odds, mask=False):
        """
        replace activations with the modified activations
        according to Cassidy's formula
        """
        modification = (
            (desired_log_odds - self.probe(activations))
            * self.probe.weight[None, :]
            / self.probe.weight.norm(2)
        )
        if mask:
            # activations.shape = (minibatch, tokens, 768)
            ones = torch.ones(activations.shape[1:], device=device)
            mask_per_mb = torch.vstack((ones[:128], ones[128:] - 1))
            mask = torch.stack([mask_per_mb for _ in range(activations.shape[0])])

            # assuming elementwise multiplication occurs
            masked_modification = mask * modification
            return activations + masked_modification
        else:
            return activations + modification

    def get_probe(self, probe_data: pd.DataFrame, device):
        # log.info(f"probe_data.shape = {probe_data.shape}")
        num_features = len(probe_data.columns) - 1
        probe = torch.nn.Linear(num_features, 1)
        np_probe_data = probe_data.to_numpy()
        # log.info(f"np_probe_data.shape = {np_probe_data.shape}")
        probe.weight = torch.nn.Parameter(
            torch.tensor(
                [np_probe_data[0][:num_features]],
                dtype=torch.float32,
                device=device,
            )
        )
        # log.info(f"probe.weight.shape = {probe.weight.shape}")
        probe.bias = torch.nn.Parameter(
            torch.tensor(
                np_probe_data[0][num_features:],
                device=device,
                dtype=torch.float32,
            )
        )
        # log.info(f"probe.bias.shape = {probe.bias.shape}")
        return probe


def plot_log_odds(log_odds, prefix_name=""):
    assert isinstance(log_odds, list)
    assert isinstance(log_odds[0], torch.Tensor)
    assert log_odds[0].shape == (256,)
    for i, sequence_log_odds in enumerate(log_odds):
        for j, _log_odds in enumerate(sequence_log_odds.cpu()):
            writer.add_scalar(f"{i}_{prefix_name}_log_odds", _log_odds, j)
    x_axis = [x for x in range(len(log_odds[0]))]
    [plt.plot(x_axis, element_log_odds.cpu()) for element_log_odds in log_odds]
    plt.savefig(f"intervene_{prefix_name}_log_odds.png")
    plt.close()


def get_intervened_model(
    base_model, intervention, index_of_intervened_layer, desired_log_odds, mask=False
):
    if mask:

        def intervene():
            def hook(model, input):
                # assumptions based on REPL testing
                # input is a tupe of one element
                # why one element? why?
                assert isinstance(input, tuple)
                assert len(input) == 1

                return intervention(input[0], desired_log_odds, mask=True)

            return hook

    else:

        def intervene():
            def hook(model, input):
                # assumptions based on REPL testing
                # input is a tupe of one element
                # why one element? why?
                assert isinstance(input, tuple)
                assert len(input) == 1

                return intervention(input[0], desired_log_odds, mask=False)

            return hook

    base_model.transformer.base_model.h[
        index_of_intervened_layer
    ].register_forward_pre_hook(intervene())
    return base_model


def log_xy_gens(xy_outputs, name="xy"):
    assert isinstance(xy_outputs, torch.Tensor)
    # looks like the first token is EOT by default
    assert xy_outputs.shape == (100, 257)
    for i, seq in enumerate(xy_outputs):
        first_half = tokenizer.decode(seq[:128])
        second_half = tokenizer.decode(seq[128:])
        log.debug(f"{name}_outputs: {first_half} | {second_half}")
        writer.add_text(f"{name}_outputs", f"{first_half} || {second_half}", i)
        writer.add_text(f"{name}_outputs_second_half_only", f"{second_half}", i)


if __name__ == "__main__":
    args = parser.parse_args()

    seed = 0
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
        device = torch.device("cuda")
    else:
        log.info("device does not support CUDA")
        device = torch.device("cpu")

    intervention_module = Intervention(args.probes_path, device)
    intervention_module.to(device)
    index_of_intervened_layer = 5
    desired_log_odds = -25
    intervened_model = get_intervened_model(
        scratch.load(args.scratch_path, device),
        intervention_module,
        index_of_intervened_layer,
        desired_log_odds,
        mask=False,
    )
    masked_intervened_model = get_intervened_model(
        scratch.load(args.scratch_path, device),
        intervention_module,
        index_of_intervened_layer,
        desired_log_odds,
        mask=True,
    )
    scratch_model = scratch.load(args.scratch_path, device)

    # test
    positive = finetuned.load_finetuned_model(args.positive, device)
    negative = finetuned.load_finetuned_model(args.negative, device)

    tokenizer = GPT2TokenizerFast.from_pretrained("gpt2")
    tokenizer.pad_token = tokenizer.eos_token

    p_outputs = positive.generate(
        do_sample=True,
        max_new_tokens=128,
        no_repeat_ngram_size=5,
        repetition_penalty=2.5,
        temperature=1.0,
        num_return_sequences=100,
    )
    pn_outputs = negative.generate(
        p_outputs,
        max_new_tokens=128,
        no_repeat_ngram_size=5,
        repetition_penalty=2.5,
        temperature=1.0,
    )
    log_xy_gens(pn_outputs, "pn")
    list_of_pn_log_odds = [
        classify.log_prob_seq_list_tensors_only(positive, pn_outputs[i], device)
        - classify.log_prob_seq_list_tensors_only(negative, pn_outputs[i], device)
        for i in range(len(pn_outputs))
    ]
    plot_log_odds(list_of_pn_log_odds, "pn")

    ps_outputs = scratch_model.generate(
        p_outputs,
        max_new_tokens=128,
        no_repeat_ngram_size=5,
        repetition_penalty=2.5,
        temperature=1.0,
    )
    log_xy_gens(ps_outputs, "ps")
    list_of_ps_log_odds = [
        classify.log_prob_seq_list_tensors_only(positive, ps_outputs[i], device)
        - classify.log_prob_seq_list_tensors_only(negative, ps_outputs[i], device)
        for i in range(len(ps_outputs))
    ]
    plot_log_odds(list_of_ps_log_odds, "ps")

    pi_outputs = intervened_model.generate(
        p_outputs,
        max_new_tokens=128,
        no_repeat_ngram_size=5,
        repetition_penalty=2.5,
        temperature=1.0,
    )
    log_xy_gens(pi_outputs, "pi")
    list_of_pi_log_odds = [
        classify.log_prob_seq_list_tensors_only(positive, pi_outputs[i], device)
        - classify.log_prob_seq_list_tensors_only(negative, pi_outputs[i], device)
        for i in range(len(pi_outputs))
    ]

    plot_log_odds(list_of_pi_log_odds, "pi")

    pmi_outputs = masked_intervened_model.generate(
        p_outputs,
        max_new_tokens=128,
        no_repeat_ngram_size=5,
        repetition_penalty=2.5,
        temperature=1.0,
    )
    log_xy_gens(pmi_outputs, "pmi")
    list_of_pmi_log_odds = [
        classify.log_prob_seq_list_tensors_only(positive, pmi_outputs[i], device)
        - classify.log_prob_seq_list_tensors_only(negative, pmi_outputs[i], device)
        for i in range(len(pmi_outputs))
    ]

    plot_log_odds(list_of_pmi_log_odds, "pmi")
