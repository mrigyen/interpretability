#!/usr/bin/env bash
# script that describes whatever experiment a specific commit is intended
# to run

export GIT_COMMIT_HASH=$(git rev-parse FETCH_HEAD)
NAS="/nas/ucb/mrigyen"
python3 -m pdb kl_divergence.py \
    --probes_path "$NAS/probes.csv" \
    --scratch_path "$NAS/train_from_scratch_6_layer_2GB_dataset/pytorch_model.bin" \
    --positive "$NAS/models/gpt-2-small/1659353389/positive/pytorch_model.bin" \
    --negative "$NAS/models/gpt-2-small/1659355055/negative/pytorch_model.bin"
