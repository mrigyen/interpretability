#!/usr/bin/env python3
"""
Generate training data for the trained-from-scratch model
"""

from transformers import GPT2LMHeadModel, GPT2TokenizerFast
import argparse
import torch
from tqdm import tqdm
import log

parser = argparse.ArgumentParser()

parser.add_argument(
    "--positive",
    dest="positive",
    type=str,
    help="path to the saved finetuned positive sentiment model",
    required=True,
)
parser.add_argument(
    "--negative",
    dest="negative",
    type=str,
    help="path to the saved finetuned negative sentiment model",
    required=True,
)

parser.add_argument(
    "--save_path",
    dest="save_path",
    type=str,
    help="path to the file in which we add text that is generated",
    required=True,
)

if __name__ == "__main__":
    try:
        # check for the existence of this variable
        __IPYTHON__
        # if no error is thrown, then we know that we are in a REPL
        args = parser.parse_args(
            [
                "--positive",
                "./models/gpt-2-small/1659353389/positive/pytorch_model.bin",
                "--negative",
                "./models/gpt-2-small/1659355055/negative/pytorch_model.bin",
                "--save_path",
                "/nas/ucb/mrigyen/3sentiment_generated_dataset",
            ]
        )
    except NameError:
        # we aren't in a REPL
        args = parser.parse_args()
    seed = 0
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
    else:
        log.info("device does not support CUDA")
    if torch.cuda.is_available():
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")

    positive = GPT2LMHeadModel.from_pretrained("gpt2")
    positive.load_state_dict(torch.load(args.positive))
    positive.eval()
    negative = GPT2LMHeadModel.from_pretrained("gpt2")
    negative.load_state_dict(torch.load(args.negative))
    negative.eval()

    if torch.cuda.is_available():
        positive.cuda()
        negative.cuda()

    tokenizer = GPT2TokenizerFast.from_pretrained("gpt2")
    tokenizer.pad_token = tokenizer.eos_token

    # not using pipeline because it does not specify the size of the output we
    # require
    # pipeline = TextGenerationPipeline(positive, tokenizer, device=1)
    with open(args.save_path + "_positive.txt", "w") as f:
        for i in tqdm(range(10000)):
            outputs = positive.generate(
                do_sample=True,
                min_length=256,
                max_length=256,
                no_repeat_ngram_size=5,
                repetition_penalty=2.5,
                temperature=1.0,
                num_return_sequences=100,
            )
            list_of_strs = tokenizer.batch_decode(outputs)
            list_of_strs = [str.replace("\n", " ") for str in list_of_strs]
            list_of_strs = [str.replace("<|endoftext|>", "\n") for str in list_of_strs]
            f.writelines(list_of_strs)

    with open(args.save_path + "_negative.txt", "w") as f:
        for i in tqdm(range(10000)):
            outputs = negative.generate(
                do_sample=True,
                min_length=256,
                max_length=256,
                no_repeat_ngram_size=5,
                repetition_penalty=2.5,
                temperature=1.0,
                num_return_sequences=100,
            )
            list_of_strs = tokenizer.batch_decode(outputs)
            list_of_strs = [str.replace("\n", " ") for str in list_of_strs]
            list_of_strs = [str.replace("<|endoftext|>", "\n") for str in list_of_strs]
            f.writelines(list_of_strs)
