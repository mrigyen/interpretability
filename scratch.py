#!/usr/bin/env python3
"""
Helper 'library' for trained from scratch models
"""

import torch
from transformers import GPT2LMHeadModel, GPT2Config

# refer to https://huggingface.co/transformers/v3.0.2/model_doc/gpt2.html
config = GPT2Config(
    # "The maximum sequence length that this model might ever be used with."
    n_positions=256,
    # "Dimensionality of the causal mask (usually same as n_positions)."
    n_ctx=256,
    # "Number of hidden layers in the Transformer encoder."
    n_layer=6,
    # "Number of attention heads for each attention layer in the Transformer encoder."
    n_head=6,
)
# using GPT2Model means model.generate doesn't work because GPT2LMHeadModel
# output is of type CausalLMOutputWithCrossAttentions which allows
# model.generate to work while GPT2Model output is of type
# BaseModelOutputWithPastAndCrossAttentions which doesn't have the `logits`
# object which model.generate() expects


def load(load_path: str, device):
    model = GPT2LMHeadModel(config=config)
    model.load_state_dict(torch.load(load_path))
    model.eval()
    model.to(device)

    return model
