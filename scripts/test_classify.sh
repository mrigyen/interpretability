#!/usr/bin/env bash

ROOT=$(git rev-parse --show-toplevel)
DIR=$ROOT/test_classify
LOG_DIR=$DIR
mkdir -p $DIR

# example is from the IMDB dataset and has a negative sentiment
python3 $ROOT/classify.py \
    --positive "$ROOT/models/gpt-2-small/1659353389/positive/pytorch_model.bin" \
    --negative "$ROOT/models/gpt-2-small/1659355055/negative/pytorch_model.bin" \
    --input "Basically there's a family where a little boy (Jake) thinks there's a zombie in his closet & his parents are fighting all the time.<br /><br />This movie is slower than a soap opera... and suddenly, Jake decides to become Rambo and kill the zombie.<br /><br />OK, first of all when you're going to make a film you must Decide if its a thriller or a drama! As a drama the movie is watchable. Parents are divorcing & arguing like in real life. And then we have Jake with his closet which totally ruins all the film! I expected to see a BOOGEYMAN similar movie, and instead i watched a drama with some meaningless thriller spots.<br /><br />3 out of 10 just for the well playing parents & descent dialogs. As for the shots with Jake: just ignore them."
