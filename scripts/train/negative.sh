#!/usr/bin/env bash

export DIR="models/gpt-2-small/$(date +%s)/"
export LOG_DIR=$DIR
mkdir -p $DIR

ROOT=$(git rev-parse --show-toplevel)
DATASET="local/datasets/IMDB Dataset.csv"

python3 $ROOT/finetune.py \
    --dataset "$ROOT/$DATASET" \
    --batch_size 4 \
    --epochs 1 \
    --save_path "$DIR/negative" \
    --sentiment "negative"
