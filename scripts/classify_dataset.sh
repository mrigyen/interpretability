#!/usr/bin/env bash

ROOT=$(git rev-parse --show-toplevel)
DIR=$ROOT/classify_dataset
LOG_DIR=$DIR
mkdir -p $DIR
python3 $ROOT/classify_dataset.py \
    --positive "$ROOT/models/gpt-2-small/1659353389/positive/pytorch_model.bin" \
    --negative "$ROOT/models/gpt-2-small/1659355055/negative/pytorch_model.bin" \
    --dataset "$ROOT/local/datasets/IMDB Dataset.csv"

mv -t $DIR *histogram.png
