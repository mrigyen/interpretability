#!/usr/bin/env bash

ROOT=$(git rev-parse --show-toplevel)
DIR=$ROOT/train_from_scratch_6_layer_13GB_dataset
LOG_DIR=$DIR
mkdir -p $DIR
DATASET_DIR="/nas/ucb/mrigyen"
python3 $ROOT/train_from_scratch.py \
    --data_dir $DATASET_DIR \
    --batch_size 8 \
    --epochs 1 \
    --save_path $DIR
