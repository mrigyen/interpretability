#!/usr/bin/env bash

CUDA_VISIBLE_DEVICES=$1 python3 generate_training_data.py \
    --positive ./models/gpt-2-small/1659353389/positive/pytorch_model.bin \
    --negative ./models/gpt-2-small/1659355055/negative/pytorch_model.bin \
    --save_path /nas/ucb/mrigyen/$1sentiment_generated_dataset
