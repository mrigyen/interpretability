#!/usr/bin/env bash

python3 train_probes.py \
    --scratch "./train_from_scratch_beta/pytorch_model.bin" \
    --positive "./models/gpt-2-small/1659353389/positive/pytorch_model.bin" \
    --negative "./models/gpt-2-small/1659355055/negative/pytorch_model.bin" \
    --trainset_path "/nas/ucb/mrigyen/100000_random_finetuned_generations.txt" \
    --testset_path "/nas/ucb/mrigyen/200_more_random_finetuned_generations.txt" \
    --use_layernorm "True" \
    --save_probes_path "./probes.bin"
