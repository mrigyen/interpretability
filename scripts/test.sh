#!/usr/bin/env bash

ROOT=$(git rev-parse --show-toplevel)
DIR=$ROOT/test
LOG_DIR=$DIR
mkdir -p $DIR
DATASET="local/datasets/sample.csv"

python3 $ROOT/finetune.py \
    --dataset "$ROOT/$DATASET" \
    --batch_size 1 \
    --epochs 1 \
    --save_path "$DIR/negative" \
    --sentiment "negative"
