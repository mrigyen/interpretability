#!/usr/bin/env python3
"""
helper function to load finetuned sentiment models
"""

import torch
from transformers import GPT2LMHeadModel


def load_finetuned_model(path, device):
    finetuned_model = GPT2LMHeadModel.from_pretrained("gpt2")
    finetuned_model.load_state_dict(torch.load(path))
    finetuned_model.eval()
    finetuned_model.to(device)
    return finetuned_model
